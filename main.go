package main

import (
	"bitbucket.org/let4be/go-crawler/crawler"
	"context"
	"fmt"
	"github.com/dustin/go-humanize"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

/*
	As this is just a test task - all configuration happens in code...
	Big picture: crawling consists of jobs, each job has individual concurrency settings, depth and can optionally follow to other domains within it's depth limit
	Ideally each job should be from the same domain, because in this implementation we have individual queues and hashmaps of visited urls per job
	we could have it globally but it's working good enough for testing as this with less lock contention

	Some notes of this particular implementation of crawler:
		- we follow only <a href="..."></a> links
		- we support both relative and absolute links
		- we ignore fragment part of the url(not being passed on the server)
		- we assume http and https on the same hostname represent exactly the same content and will never visit https version of the page if we already visited http(and vice versa)
		- used a little hackish version of job stop condition, but again without precise requirements for this task my implementation seems to be working good enough for testing purposes
		- signal handling and cancellation support...
		- custom user agent and headers supported...
		- no cookies support obviously yet, but it's not a problem ;)
 */

var Jobs = []*crawler.JobParams{
	{URL: "http://habr.com", Depth: 2, AllowForeign: false, Concurrency: 10},
	{URL: "https://bash.im", Depth: 2, AllowForeign: false, Concurrency: 5},
	{URL: "https://stackoverflow.com", Depth: 2, AllowForeign: false, Concurrency: 10},
}

var c *crawler.Crawler

func SetupInterruptHandler() context.Context {
	ctx, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("\r- Ctrl+C pressed in Terminal")
		cancel()
	}()

	return ctx
}

func main() {
	c = crawler.New(2)

	c.SetUserAgent("Super Bot 1.0")
	c.SetCustomHeader(map[string][]string{
		"Test": {"It"},
	})

	ctx := SetupInterruptHandler()

	for _, jp := range Jobs {
		if err := c.AddJob(jp); err != nil {
			fmt.Println(fmt.Sprintf("Cannot add job for %s: %w", jp.URL, err))
		} else {
			fmt.Println(fmt.Sprintf("Job added: %s", jp.URL))
		}
	}

	fmt.Println("Starting crawling jobs...")
	ch := c.Go(ctx)

	var wg sync.WaitGroup
	wg.Add(1)
	go func(){
		defer wg.Done()

		for c := range ch {
			if c.Error != nil {
				fmt.Println(fmt.Sprintf("[ERR] %s: %v", c.URL, c.Error))
			} else {
				fmt.Println(fmt.Sprintf("[%d %d] %s: %s with %d link(s)", c.ResponseCode, c.Level, c.URL, humanize.Bytes(uint64(c.PageSize)), c.LinksFound))
			}
		}
	}()

	fmt.Println("Awaiting stop conditions...")
	c.Wait()
	wg.Wait()

	fmt.Println("Crawling finished!")
	s := c.Stats()
	fmt.Println("Links processed: ", s.LinksProcessed)
	fmt.Println("Pages visited: ", s.PagesVisited)
	fmt.Println("Downloaded: ", humanize.Bytes(uint64(s.Downloaded)))
}
