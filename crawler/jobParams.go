package crawler

import (
	"fmt"
	"net/url"
	"strings"
)

type JobParams struct {
	URL string			// entry point
	Depth int			// how deep do we want to dig?
	Concurrency int		// how many green threads do we want working in parallel on this job?
	AllowForeign bool	// allows to go broad crawling but with current implementation it's a bad idea... depth limit saves the day tho

	u *url.URL
	host string
}

func (jp *JobParams) Init() error {
	if jp.Concurrency < 1 {
		return fmt.Errorf("concurrency should be >= 1, given: %d", jp.Concurrency)
	}

	u, err := url.Parse(jp.URL)
	if err != nil {
		return fmt.Errorf("invalid URL %s: %w", jp.URL, err)
	}

	jp.u = u
	jp.host = strings.ToLower(u.Host)

	return nil
}
