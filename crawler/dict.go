package crawler

import (
	"net/url"
	"strings"
	"sync"
)

type dictMap map[string]struct{}

type dict struct {
	hash dictMap
	m sync.Mutex
}

func newDict() *dict {
	return &dict{hash: make(dictMap)}
}

func (d *dict) checkVisitedBefore(url *url.URL) bool {
	uc, _ := url.Parse(url.String())
	uc.Scheme = "http"

	d.m.Lock()
	defer d.m.Unlock()

	uHashString := strings.TrimRight(strings.TrimSpace(uc.String()), "/")
	if _, exists := d.hash[uHashString]; exists {
		return false
	}
	d.hash[uHashString] = struct{}{}
	return true
}