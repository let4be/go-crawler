package crawler

import (
	"context"
	"fmt"
	"net/http"
	"sync"
)

type Crawler struct {
	jobConcurrency int	// How many jobs keep open in parallel with others waiting...
	feed Feed

	customHeaders http.Header

	jobs []*job
	lim *limit
	wg sync.WaitGroup
	s *stats
}


type Response struct {
	URL string			// URL visited
	Error error			// Errors during URL processing

	*ResponseData
}

type ResponseData struct {
	ResponseCode int	// Response Code
	PageSize int		// Total page size in bytes
	LinksFound int		// How many links were found and followed
	Level int			// Depth level
}

type Feed chan *Response

func New(jobConcurrency int) *Crawler {
	return &Crawler{
		jobConcurrency: jobConcurrency,
		feed: make(Feed, 10),
		lim: newLimit(jobConcurrency),
		s: newStats(),
		customHeaders: make(http.Header),
	}
}

func (c *Crawler) SetUserAgent(ug string) {
	c.customHeaders["User-Agent"] = []string{ug}
}

func (c *Crawler) SetCustomHeader(h http.Header) {
	for n, v := range h {
		c.customHeaders[n] = v
	}
}

func (c *Crawler) AddJob(p *JobParams) error {
	if err := p.Init(); err != nil {
		return fmt.Errorf("invalid job description: %w", err)
	}

	job := newJob(c.feed, c.s, p, c.customHeaders)
	c.jobs = append(c.jobs, job)

	return nil
}

func (c *Crawler) Go(ctx context.Context) Feed {
	c.wg.Add(len(c.jobs) + 1)

	go func() {
		defer c.wg.Done()

		for _, j := range c.jobs {
			if !c.lim.Acquire(ctx) {
				c.wg.Done()
				continue
			}

			go func(j *job) {
				defer func() {
					c.lim.Release()
					c.wg.Done()
				}()

				j.Go(ctx)
			}(j)
		}
	}()

	return c.feed
}

func (c *Crawler) Stats() *StatsSnapshot {
	return c.s.Snapshot()
}

func (c *Crawler) Wait() {
	c.wg.Wait()
	close(c.feed)
}