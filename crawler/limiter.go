package crawler

import "context"

type limit struct {
	ch chan struct{}
}

func newLimit(cap int) *limit {
	return &limit{ch: make(chan struct{}, cap)}
}

func (lim *limit) Acquire(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return false
	case lim.ch <- struct {}{}:
		return true
	}
}

func (lim *limit) Release() {
	<-lim.ch
}