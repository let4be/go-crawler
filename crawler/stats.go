package crawler

import "github.com/rcrowley/go-metrics"

type stats struct {
	LinksProcessed metrics.Counter
	PagesVisited metrics.Counter
	Downloaded metrics.Counter
}

func newStats() *stats {
	return &stats{
		LinksProcessed: metrics.NewCounter(),
		PagesVisited:   metrics.NewCounter(),
		Downloaded:     metrics.NewCounter(),
	}
}

type StatsSnapshot struct {
	LinksProcessed int64
	PagesVisited int64
	Downloaded int64
}

func (s *stats) TrackLinks(links int) {
	s.LinksProcessed.Inc(int64(links))
}

func (s *stats) TrackPageVisit(pageSize int) {
	s.PagesVisited.Inc(1)
	s.Downloaded.Inc(int64(pageSize))
}

func (s *stats) Snapshot() *StatsSnapshot {
	return &StatsSnapshot{
		LinksProcessed: s.LinksProcessed.Count(),
		PagesVisited:   s.PagesVisited.Count(),
		Downloaded:      s.Downloaded.Count(),
	}
}

