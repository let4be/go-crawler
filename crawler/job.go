package crawler

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"
)

var URLRegex = regexp.MustCompile(`(?i)<a\s+(?:[^>]*?\s+)?href="([^"]*)"`)

type queue chan *queuedItem

type queuedItem struct {
	u *url.URL
	level int
}

type job struct {
	*JobParams
	context.Context

	feed Feed
	http *http.Client
	dict *dict

	queue queue
	s *stats
	customHeader http.Header
}

func newJob(feed Feed, stats *stats, params *JobParams, customHeader http.Header) *job {
	return &job{
		JobParams: params,

		feed: feed,
		http: &http.Client{},
		dict: newDict(),
		queue: make(queue, 1000),
		s: stats,
		customHeader: customHeader,
	}
}

func (j *job) isAllowed(u *url.URL, level int) bool {
	if level >= j.Depth {
		return false
	}

	if j.AllowForeign || strings.ToLower(u.Host) == j.host {
		return j.dict.checkVisitedBefore(u)
	}

	return false
}

func (j *job) enqueue(u *url.URL, level int) {
	j.queue <- &queuedItem{u: u, level: level+1}
}

func (j *job) handleQueue(ctx context.Context) {
	for {
		t := time.NewTimer(time.Second * 3)
		select {
		case q := <-j.queue:
			rd, err := j.Execute(q.u, q.level)

			j.feed <- &Response{
				URL:          q.u.String(),
				Error:        err,
				ResponseData: rd,
			}
			case <-ctx.Done():
				return
			case <-t.C:
				return
		}
	}
}

func (j *job) Go(ctx context.Context) {
	j.dict.checkVisitedBefore(j.u)
	rd, err := j.Execute(j.u, 0)

	j.feed <- &Response{
		URL:          j.u.String(),
		Error:        err,
		ResponseData: rd,
	}

	var wg sync.WaitGroup
	wg.Add(j.Concurrency)

	for i := 0; i < j.Concurrency; i++ {
		go func(){
			defer wg.Done()
			j.handleQueue(ctx)
		}()
	}

	wg.Wait()
}

func (j *job) parseAndResolveHrefs(body string, level int) []*url.URL {
	var urls []*url.URL
	matches := URLRegex.FindAllStringSubmatch(body, -1)
	for _, m := range matches {
		if len(m) < 2 {
			continue
		}

		href := m[1]
		u, err := url.Parse(href)
		if err != nil {
			continue
		}
		u.Fragment = ""

		if !u.IsAbs() {
			u = j.u.ResolveReference(u)
		}
		s := strings.ToLower(u.Scheme)
		if !strings.HasPrefix(s, "http") {
			continue
		}

		if j.isAllowed(u, level) {
			urls = append(urls, u)
		}
	}

	j.s.TrackLinks(len(matches))
	return urls
}

func (j *job) Execute(ju *url.URL, level int) (*ResponseData, error) {
	jUrlStr := ju.String()

	req, err := http.NewRequest("GET", jUrlStr, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot create GET on %s: %w", jUrlStr, err)
	}
	req.Header = j.customHeader

	res, err := j.http.Do(req)
	if err != nil {
		return nil, fmt.Errorf("cannot execute GET on %s: %w", jUrlStr, err)
	}
	defer func() {
		_ = res.Body.Close()
	}()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("cannot read response on GET of %s: %w", jUrlStr, err)
	}

	j.s.TrackPageVisit(len(body))
	urls := j.parseAndResolveHrefs(string(body), level)

	// we can't lock here
	go func(){
		for _, u := range urls {
			j.enqueue(u, level)
		}
	}()

	return &ResponseData{
		ResponseCode: res.StatusCode,
		PageSize:     len(body),
		LinksFound:   len(urls),
		Level:        level,
	}, nil
}