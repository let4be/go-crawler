module bitbucket.org/let4be/go-crawler

go 1.13

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/rcrowley/go-metrics v0.0.0-20190826022208-cac0b30c2563
)
